# README #

All IT eBooks Scraper

* A Python based scraper for  web crawling basics
* Version: 1.0

### Requirements ###

* Requests (pip install requests)
* BeautifulSoup4 (pip install beautifulsoup4)
* Some hard disk space (at least 100GB)

Just call: python allit_scrapper.py
And download