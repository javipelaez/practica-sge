# -*- coding: utf-8 -*-
import requests, pdb, traceback, os, json, time
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup

class AllItScrapper():
    links = []

    def __init__(self):
        print("Init Scrapper")

    def get_specific_url_book(self):
        print("Getting urls")
        for x in xrange(1, 670):
            # The first page /page/1 will redirect to base URL
            if x == 1:
                url = 'http://www.allitebooks.com'
            else:
                url = 'http://www.allitebooks.com/page/%s/' % str(x)
            r  = requests.get(url) # Calls the URL
            soup = BeautifulSoup(r.text, 'html.parser') # Convert HTML code into an object
            for element in soup.find_all('h2', class_='entry-title'):
                # Append a dictonary to a list
                self.links.append({
                    'title': element.a.get('href').replace('http://www.allitebooks.com/','').replace('/',''),
                    'url': element.a.get('href')
                    })
        print("Got the urls")
        return True

    def download_file_threads(self, obj):
        try:
            r = requests.get(obj['url'])
            res = r.text
            soup = BeautifulSoup(res, 'html.parser')
            r1 = requests.get(soup.find_all('span', class_='download-links')[0].a.get('href'))
            name = 'downloads/%s.pdf' % obj['title']
            with open(name, 'wb') as downloaded_file:
                downloaded_file.write(r1.content)
        except Exception as e:
            print(traceback.format_exc())
            print("Failed to call %s" % obj['url'])
        return True

    def download_files_from_links(self, links = None):
        # If a list is provided in the same format [{'title':'title', 'url':'url'}]
        # It will use that list, if not will use the 
        if not links:
            links = self.links
        t0 = time.time()
        print("Downloading files")
        pool = ThreadPool(processes = 10)
        pool.map(self.download_file_threads, links)
        print("Completed in :")
        print(time.time()-t0)
        return True

    def save_json_info(self):
        with open('data.json', 'w') as outfile:
            json.dump(self.links, outfile)
        print("Generated json file")
        return True


def run_application(run_forever = None):
    print(u"CTRL+C para exit")
    s = AllItScrapper()
    try:
        s.links = json.load(open('data.json'))
        print("Valid JSON")
    except Exception as e:
        print("Json not valid Getting URLs")
        s.get_specific_url_book()
        s.save_json_info()
    s.download_files_from_links()
    while run_forever:
        pass
    return True


if __name__ == '__main__':
    run_application()